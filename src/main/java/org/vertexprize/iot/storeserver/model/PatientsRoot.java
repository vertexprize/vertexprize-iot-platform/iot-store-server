/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.iot.storeserver.model;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author vaganovdv
 */

public class PatientsRoot {
    
    private  List<Patient> patients = new ArrayList(); 

    public PatientsRoot() {
        super();
    }

    
    
    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

}
