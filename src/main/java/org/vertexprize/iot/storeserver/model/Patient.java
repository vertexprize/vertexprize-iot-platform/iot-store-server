/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.iot.storeserver.model;


import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author vaganovdv
 */

public class Patient {
        
    private String name;
    private LocalDate  bithDate;
    private List<String> diagnosis = new ArrayList<>();     

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBithDate() {
        return bithDate;
    }

    public void setBithDate(LocalDate bithDate) {
        this.bithDate = bithDate;
    }

    public List<String> getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(List<String> diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Patient{");
        sb.append("name=").append(name);
        sb.append(", bithDate=").append(bithDate);
        sb.append(", diagnosis=").append(diagnosis);
        sb.append('}');
        return sb.toString();
    }
    
    
    
}
