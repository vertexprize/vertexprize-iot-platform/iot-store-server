/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.iot.storeserver.config;

import org.eclipse.store.integrations.spring.boot.types.configuration.EclipseStoreProperties;
import org.eclipse.store.integrations.spring.boot.types.factories.EmbeddedStorageFoundationFactory;
import org.eclipse.store.integrations.spring.boot.types.factories.EmbeddedStorageManagerFactory;
import org.eclipse.store.storage.embedded.types.EmbeddedStorageFoundation;
import org.eclipse.store.storage.embedded.types.EmbeddedStorageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.vertexprize.iot.storeserver.model.PatientsRoot;

/**
 *
 * @author vaganovdv
 */
@Configuration
public class IotStoreConfig {

    private static final Logger log = LoggerFactory.getLogger(IotStoreConfig.class);

    private final EmbeddedStorageFoundationFactory foundationFactory;
    private final EmbeddedStorageManagerFactory managerFactory;

    public IotStoreConfig(EmbeddedStorageFoundationFactory foundationFactory, EmbeddedStorageManagerFactory managerFactory) {
        this.foundationFactory = foundationFactory;
        this.managerFactory = managerFactory;
    }

    @Bean("patients")
    @ConfigurationProperties("org.eclipse.store.patients")
    public EclipseStoreProperties patientProperties() {
        return new EclipseStoreProperties();
    }

    @Bean
    @Qualifier("patients")
    public EmbeddedStorageManager patientStore(@Qualifier("patients") final EclipseStoreProperties p) {
        EmbeddedStorageFoundation<?> foundation = foundationFactory.createStorageFoundation(p);

        final EmbeddedStorageManager storageManager = foundation.createEmbeddedStorageManager().start();

        if (storageManager.root() == null) {
            log.info("Корневой объект не существует создание ....");
            PatientsRoot data = new PatientsRoot();
            storageManager.setRoot(data);
            storageManager.storeRoot();
            log.info("Корневой объект СОЗДАН");
        } else {
            PatientsRoot database = (PatientsRoot) storageManager.root();
            log.info("--------------------------------------------------------");
            log.info("База данных содержит [" + database.getPatients().size() + "]  записей");
            log.info("--------------------------------------------------------\n");
        }

        return storageManager;

    }

}
