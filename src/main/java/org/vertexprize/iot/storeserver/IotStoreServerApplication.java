package org.vertexprize.iot.storeserver;

import java.util.UUID;
import java.time.LocalDate;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import java.time.Month;
import java.util.List;
import java.util.stream.Collectors;
import org.eclipse.store.storage.embedded.types.EmbeddedStorageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.vertexprize.iot.storeserver.manager.PatientStoreManager;
import org.vertexprize.iot.storeserver.model.Patient;
import org.vertexprize.iot.storeserver.model.PatientsRoot;

@SpringBootApplication
public class IotStoreServerApplication {

    private static final Logger log = LoggerFactory.getLogger(IotStoreServerApplication.class);
    
    @Autowired
    @Qualifier("patients")
    private EmbeddedStorageManager storageManager;
    
    @Autowired
    private PatientStoreManager patientStoreManager;

    public static void main(String[] args) {
        SpringApplication.run(IotStoreServerApplication.class, args);
    }

  

    @PostConstruct
    public void start() {
        log.info("[" + IotStoreServerApplication.class.getSimpleName() + "] Старт сервера ...");
        String dbname = storageManager.databaseName();  
        
        printStatus();
        
        PatientsRoot root = new PatientsRoot();
        
        
        log.info(String.format("%-35s %s", "Имя базы данных: ",  dbname));
        int size = patientStoreManager.getAllPatients().size();
        
        log.info(String.format("%-35s %s", "База данных пациентов ",  "["+size+"] записей"));
        test();
        
    }

    @PreDestroy
    public void stop() {
        log.info("[" + this.getClass().getSimpleName() + "] Остановка сервера ...");
        storageManager.shutdown();
        printStatus();
        log.info("[" + IotStoreServerApplication.class.getSimpleName() + "] Сервер остановлен");
        int size = patientStoreManager.getAllPatients().size();        
        log.info(String.format("%-35s %s", "База данных пациентов ",  "["+size+"] записей"));

    }

    private void printStatus() {
        boolean active = storageManager.isActive();
        boolean running = storageManager.isRunning();
        boolean acceptingTasks = storageManager.isAcceptingTasks();
        log.info("--------------------------------------------------------");
        log.info(String.format("%-35s %s", "Активность сервера: ", "[" + active + "]"));
        log.info(String.format("%-35s %s", "Статус: ", "[" + running + "]"));
        log.info(String.format("%-35s %s", "Готовность к приему заданий:", "[" + acceptingTasks + "]"));
        log.info("--------------------------------------------------------");

    }
    
   
    private void test()
    {
        log.info("Тесты записи и чтения базы пациентов ...");
        
        Patient p1 = new Patient();
        p1.setName(UUID.randomUUID().toString());
        p1.setBithDate(LocalDate.of(1972, Month.MARCH, 19));        
        p1.getDiagnosis().add("Мерцательная аритмия");
        
        Patient p2 = new Patient();
        p2.setName(UUID.randomUUID().toString());
        p2.setBithDate(LocalDate.of(2005, Month.AUGUST, 1));        
        p2.getDiagnosis().add("Брадикардия");
        
        Patient p3 = new Patient();
        p3.setName(UUID.randomUUID().toString());
        p3.setBithDate(LocalDate.of(1992, Month.FEBRUARY, 3));        
        p3.getDiagnosis().add("Брадикардия");
        
        
        Patient p4 = new Patient();
        p4.setName(UUID.randomUUID().toString());
        p4.setBithDate(LocalDate.of(1985, Month.FEBRUARY, 10));        
        p4.getDiagnosis().add("Эндокардит");
        
        
        patientStoreManager.addPatient(p1);
        patientStoreManager.addPatient(p2);
        patientStoreManager.addPatient(p3);
        patientStoreManager.addPatient(p4);
        
        
        patientStoreManager.getAllPatients();
        
        log.info("Тесты поиска в базе данных...");
        log.info("Поиск  пациентов с диагнозом Брадикардия ...");
        
        PatientsRoot root =  (PatientsRoot) storageManager.root();
        
        List<Patient> br = root.getPatients().stream().filter( p -> p.getDiagnosis().contains("Брадикардия")).collect(Collectors.toList());
        log.info("Обнаружено ["+br.size()+"] пациентов с диагнозом [Брадикардия]");
        
        
        List<Patient> en = root.getPatients().stream().filter( p -> p.getDiagnosis().contains("Эндокардит")).collect(Collectors.toList());
        log.info("Обнаружено ["+en.size()+"] пациентов с диагнозом [Эндокардит]");
        
        List<Patient> ma = root.getPatients().stream().filter( p -> p.getDiagnosis().contains("Мерцательная аритмия")).collect(Collectors.toList());
        log.info("Обнаружено ["+ma.size()+"] пациентов с диагнозом [Мерцательная аритмия]");
        
        
    }
    
    

}
