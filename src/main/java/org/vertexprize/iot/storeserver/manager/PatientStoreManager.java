/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.iot.storeserver.manager;

import java.util.List;
import java.util.ArrayList;
import org.eclipse.serializer.concurrency.XThreads;
import org.eclipse.store.storage.embedded.types.EmbeddedStorageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.vertexprize.iot.storeserver.model.PatientsRoot;
import org.vertexprize.iot.storeserver.model.Patient;
import org.eclipse.store.integrations.spring.boot.types.concurrent.Write;

/**
 *
 * @author vaganovdv
 */
@Service
public class PatientStoreManager {

    private static final Logger log = LoggerFactory.getLogger(PatientStoreManager.class);

    private final EmbeddedStorageManager storageManager;

    public PatientStoreManager( @Qualifier("patients") EmbeddedStorageManager storageManager) {
        this.storageManager = storageManager;
    }

    /**
     * Получение полного списка пациентов
     *
     * @return
     */
    public List<Patient> getAllPatients() {
        List<Patient> patients = new ArrayList<>();
        log.info("Получение полного списка пациентов ...");

        XThreads.executeSynchronized(() -> {
            PatientsRoot root = (PatientsRoot) storageManager.root();
            patients.addAll(root.getPatients());
        });

        log.info("Получен список [" + patients.size() + "]  пациентов");
        return patients;
    }

    @Write
    public void addPatient(Patient patient) {
        log.info("Добавление пациента в базу данных ...");

        XThreads.executeSynchronized(() -> {
            PatientsRoot root = (PatientsRoot) storageManager.root();
            root.getPatients().add(patient);                        
            long count = storageManager.storeRoot();            
            log.info("Добавлен пациент:  "+patient.getName()+ "["+count+"] объектов");
        });
    }

}
